Name:          atk
Version:       2.38.0
Release:       1
Summary:       Interfaces for accessibility support
License:       LGPLv2+
URL:           http://developer.gnome.org/platform-overview/stable/atk
Source0:       http://download.gnome.org/sources/atk/2.36/atk-%{version}.tar.xz

BuildRequires: glib2-devel gettext gobject-introspection-devel gtk-doc meson libxslt

%description
The Accessibility Toolkit(ATK) is an open source software library, part of
the GNOME project, which provides application programming interfaces for
implementing accessibility support in software.

%package       devel
Summary:       Development files for the ATK accessibility toolkit
Requires:      %{name}%{?_isa} = %{version}-%{release}

%description   devel
This package provides libraries, header files, and developer documentation
needed for ATK related applications or toolkits.

%prep
%autosetup -p1

%build
%meson -Ddocs=true
%meson_build

%check
%meson_test

%install
%meson_install
sed -i 's/idm[0-9]\{5,32\}\"/idm12345678912345\"/g' $(find %{_buildrootdir} -name api-index-full.html)

%find_lang atk10
%files -f atk10.lang
%license COPYING
%doc README AUTHORS NEWS
%{_libdir}/libatk-1.0.so.*
%{_libdir}/girepository-1.0

%files         devel
%{_includedir}/atk-1.0
%{_libdir}/libatk-1.0.so
%{_libdir}/pkgconfig/atk.pc
%{_datadir}/gtk-doc/html/atk
%{_datadir}/gir-1.0

%changelog
* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 2.38.0-1
- Upgrade to 2.38.0

* Mon Apr 11 2022 yangcheng <yangcheng87@h-partners.com> - 2.36.0-2
- enable check while building

* Mon Jul 20 2020 wangye <wangye70@huawei.com> - 2.36.0-1
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:version update

* Mon Nov 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.30.0-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add the libxslt in buildrequires

* Tue Sep 10 2019 Alex Chao <zhaolei746@huawei.com> - 2.30.0-2
- Package init

